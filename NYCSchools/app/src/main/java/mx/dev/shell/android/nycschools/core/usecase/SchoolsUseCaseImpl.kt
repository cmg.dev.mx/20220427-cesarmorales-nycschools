package mx.dev.shell.android.nycschools.core.usecase

import kotlinx.coroutines.flow.Flow
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.core.repository.SchoolRepository
import javax.inject.Inject

class SchoolsUseCaseImpl @Inject constructor(
    private val repository: SchoolRepository
) : SchoolsUseCase {

    override suspend fun querySchools(): Flow<Result<List<SchoolBo>>> = repository.querySchools()
}