package mx.dev.shell.android.nycschools.web.model.sat.response


import com.google.gson.annotations.SerializedName

data class QuerySchoolsScoresResponseItem(
    @SerializedName("dbn")
    var dbn: String? = null,
    @SerializedName("num_of_sat_test_takers")
    var numOfSatTestTakers: String? = null,
    @SerializedName("sat_critical_reading_avg_score")
    var satCriticalReadingAvgScore: String? = null,
    @SerializedName("sat_math_avg_score")
    var satMathAvgScore: String? = null,
    @SerializedName("sat_writing_avg_score")
    var satWritingAvgScore: String? = null,
    @SerializedName("school_name")
    var schoolName: String? = null
)