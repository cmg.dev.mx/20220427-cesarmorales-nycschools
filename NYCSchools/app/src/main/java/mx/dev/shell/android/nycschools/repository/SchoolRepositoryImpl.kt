package mx.dev.shell.android.nycschools.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.core.model.SchoolScoreBo
import mx.dev.shell.android.nycschools.core.repository.SchoolRepository
import mx.dev.shell.android.nycschools.repository.mapper.SchoolMapper
import mx.dev.shell.android.nycschools.web.service.NycSchoolsService
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(
    private val service: NycSchoolsService,
    private val mapper: SchoolMapper
) : SchoolRepository {

    override suspend fun querySchools(): Flow<Result<List<SchoolBo>>> {
        return service.getSchools().map {
            if (it.isSuccess) {
                // Map the respone object to a business logic object
                Result.success(mapper.invoke(it.getOrNull()!!))
            } else {
                // If service fails, then return the error message
                Result.failure(it.exceptionOrNull()!!)
            }
        }.combine(service.getSchoolsScores()) { resultSchools, resultScores ->
            // Call the other service with the SAT scores and merge them with the previous mapped response
            if (resultSchools.isSuccess && resultScores.isSuccess) {
                resultSchools.getOrNull()?.forEach { schoolBo ->
                    resultScores.getOrNull()?.let { scores ->
                        scores.firstOrNull {
                            it.dbn == schoolBo.id
                        }?.let { score ->
                            schoolBo.scores = SchoolScoreBo(
                                scrRead = score.satCriticalReadingAvgScore?.toIntOrNull()?:0,
                                scrMath = score.satMathAvgScore?.toIntOrNull()?:0,
                                scrWrite = score.satWritingAvgScore?.toIntOrNull()?:0
                            )
                        }?:let { // If the score for the school is null, then create with 0
                            schoolBo.scores = SchoolScoreBo()
                        }
                    }
                }
                Result.success(resultSchools.getOrNull()!!)
            } else {
                Result.failure(resultSchools.exceptionOrNull()!!)
            }
        }
    }
}