package mx.dev.shell.android.nycschools.web.model.schools.response


import com.google.gson.annotations.SerializedName

data class QuerySchoolsResponseItem(
    @SerializedName("dbn")
    var dbn: String? = null,
    @SerializedName("interest1")
    var interest1: String? = null,
    @SerializedName("latitude")
    var latitude: String? = null,
    @SerializedName("location")
    var location: String? = null,
    @SerializedName("longitude")
    var longitude: String? = null,
    @SerializedName("phone_number")
    var phoneNumber: String? = null,
    @SerializedName("school_email")
    var schoolEmail: String? = null,
    @SerializedName("school_name")
    var schoolName: String? = null,
    @SerializedName("website")
    var website: String? = null
)