package mx.dev.shell.android.nycschools.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import mx.dev.shell.android.nycschools.core.repository.SchoolRepository
import mx.dev.shell.android.nycschools.core.usecase.SchoolsUseCase
import mx.dev.shell.android.nycschools.core.usecase.SchoolsUseCaseImpl
import mx.dev.shell.android.nycschools.repository.SchoolRepositoryImpl
import mx.dev.shell.android.nycschools.web.api.NycSchoolsApi
import mx.dev.shell.android.nycschools.web.api.client.RetrofitApiClient
import mx.dev.shell.android.nycschools.web.service.NycSchoolsService
import mx.dev.shell.android.nycschools.web.service.NycSchoolsServiceImpl
import retrofit2.create

@Module
@InstallIn(FragmentComponent::class)
abstract class SchoolsModule {

    @Binds
    abstract fun bindSchoolsUseCase(impl: SchoolsUseCaseImpl): SchoolsUseCase

    @Binds
    abstract fun bindSchoolRepository(impl: SchoolRepositoryImpl): SchoolRepository

    @Binds
    abstract fun bindNycSchoolsService(impl: NycSchoolsServiceImpl): NycSchoolsService
}