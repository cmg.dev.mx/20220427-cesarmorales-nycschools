package mx.dev.shell.android.nycschools.core.model

import android.os.Parcel
import android.os.Parcelable

data class LocationBo(
    val latitude: Double,
    val longitude: Double
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LocationBo> {
        override fun createFromParcel(parcel: Parcel): LocationBo {
            return LocationBo(parcel)
        }

        override fun newArray(size: Int): Array<LocationBo?> {
            return arrayOfNulls(size)
        }
    }
}
