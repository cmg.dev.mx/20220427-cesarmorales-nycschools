package mx.dev.shell.android.nycschools.flow.schools.vm

import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.core.usecase.SchoolsUseCase
import javax.inject.Inject

class SchoolListViewModel @Inject constructor(
    private val useCase: SchoolsUseCase
): ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val showProgressBar = MutableLiveData<Boolean>()

    val schools = liveData {
        showProgressBar.postValue(true)
        emitSource(
            useCase.querySchools()
                .onEach {
                    showProgressBar.postValue(false)
                    if (it.isSuccess) {
                        errorMessage.postValue("")
                    } else {
                        errorMessage.postValue(it.exceptionOrNull()?.message ?: "Something went wrong")
                    }
                }.asLiveData()
        )
    }


}