package mx.dev.shell.android.nycschools.core.repository

import kotlinx.coroutines.flow.Flow
import mx.dev.shell.android.nycschools.core.model.SchoolBo

interface SchoolRepository {
    suspend fun querySchools(): Flow<Result<List<SchoolBo>>>
}