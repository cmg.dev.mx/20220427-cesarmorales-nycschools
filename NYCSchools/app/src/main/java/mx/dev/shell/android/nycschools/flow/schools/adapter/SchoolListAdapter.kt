package mx.dev.shell.android.nycschools.flow.schools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.databinding.ItemSchoolBinding

class SchoolListAdapter(
    private val list: ArrayList<SchoolBo>,
    private val itemSelectedListener: (SchoolBo) -> Unit
) : RecyclerView.Adapter<SchoolListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(list[position], itemSelectedListener)

    override fun getItemCount() = list.size

    fun setNewList(newList: List<SchoolBo>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    class ViewHolder(
        private val binding: ItemSchoolBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(school: SchoolBo, itemSelectedListener: (SchoolBo) -> Unit) {
            binding.itemSchoolName.text = school.name
            binding.itemSchoolDescription.text = school.schoolType
            binding.itemSchoolContainer.setOnClickListener {
                itemSelectedListener(school)
            }
        }
    }
}