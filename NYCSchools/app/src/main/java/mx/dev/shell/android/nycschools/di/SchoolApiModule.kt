package mx.dev.shell.android.nycschools.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import mx.dev.shell.android.nycschools.web.api.NycSchoolsApi
import mx.dev.shell.android.nycschools.web.api.client.RetrofitApiClient

@Module
@InstallIn(FragmentComponent::class)
class SchoolApiModule {

    @Provides
    fun providesApi() = RetrofitApiClient.getApiClient().create(NycSchoolsApi::class.java)
}