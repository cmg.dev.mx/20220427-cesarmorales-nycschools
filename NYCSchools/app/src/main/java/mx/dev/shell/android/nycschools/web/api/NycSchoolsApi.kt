package mx.dev.shell.android.nycschools.web.api

import mx.dev.shell.android.nycschools.web.model.sat.response.QuerySchoolsScoresResponseItem
import mx.dev.shell.android.nycschools.web.model.schools.response.QuerySchoolsResponseItem
import retrofit2.http.GET

interface NycSchoolsApi {

    @GET("s3k6-pzi2.json")
    suspend fun querySchools(): List<QuerySchoolsResponseItem>

    @GET("f9bf-2cp4.json")
    suspend fun querySchoolsScores(): List<QuerySchoolsScoresResponseItem>
}