package mx.dev.shell.android.nycschools.core.usecase

import kotlinx.coroutines.flow.Flow
import mx.dev.shell.android.nycschools.core.model.SchoolBo

interface SchoolsUseCase {
    suspend fun querySchools(): Flow<Result<List<SchoolBo>>>
}