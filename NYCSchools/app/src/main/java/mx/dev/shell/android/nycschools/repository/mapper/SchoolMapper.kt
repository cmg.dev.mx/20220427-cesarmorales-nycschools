package mx.dev.shell.android.nycschools.repository.mapper

import mx.dev.shell.android.nycschools.core.model.ContactInfoBo
import mx.dev.shell.android.nycschools.core.model.LocationBo
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.web.model.schools.response.QuerySchoolsResponseItem
import javax.inject.Inject

class SchoolMapper @Inject constructor(): Function1<List<QuerySchoolsResponseItem>, List<SchoolBo>> {

    override fun invoke(p1: List<QuerySchoolsResponseItem>): List<SchoolBo> {
        return p1.map {
            SchoolBo(
                contactInfo = ContactInfoBo(
                    address = it.location?:"",
                    email = it.schoolEmail?:"",
                    phone = it.phoneNumber?:"",
                    webSite = it.website?:""
                ),
                id = it.dbn?:"",
                location = LocationBo(
                    latitude = it.latitude?.toDouble()?:0.0,
                    longitude = it.longitude?.toDouble()?:0.0
                ),
                name = it.schoolName?:"",
                schoolType = it.interest1?:""
            )
        }
    }
}
