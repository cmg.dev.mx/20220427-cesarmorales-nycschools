package mx.dev.shell.android.nycschools.web.api.client

import mx.dev.shell.android.nycschools.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitApiClient {

    fun getApiClient(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(getClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getClient(): OkHttpClient {
        val okHttpInterceptor = HttpLoggingInterceptor()
            .setLevel(
                if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor.Level.BODY
                }
                else {
                    HttpLoggingInterceptor.Level.NONE
                }
            )
        return OkHttpClient.Builder()
            .addInterceptor(okHttpInterceptor)
            .build()
    }
}