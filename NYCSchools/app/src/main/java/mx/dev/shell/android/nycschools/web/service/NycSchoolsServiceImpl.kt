package mx.dev.shell.android.nycschools.web.service

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import mx.dev.shell.android.nycschools.web.api.NycSchoolsApi
import mx.dev.shell.android.nycschools.web.model.sat.response.QuerySchoolsScoresResponseItem
import mx.dev.shell.android.nycschools.web.model.schools.response.QuerySchoolsResponseItem
import java.lang.RuntimeException
import javax.inject.Inject

class NycSchoolsServiceImpl @Inject constructor(
    private val api: NycSchoolsApi
) : NycSchoolsService {

    override suspend fun getSchools(): Flow<Result<List<QuerySchoolsResponseItem>>> {
        return flow {
            emit(Result.success(api.querySchools()))
        }.catch {
            emit(Result.failure(RuntimeException("Something went wrong!")))
        }
    }

    override suspend fun getSchoolsScores(): Flow<Result<List<QuerySchoolsScoresResponseItem>>> {
        return flow {
            emit(Result.success(api.querySchoolsScores()))
        }.catch {
            emit(Result.failure(RuntimeException("Something went wrong!")))
        }
    }
}