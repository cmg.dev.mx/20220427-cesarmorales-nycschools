package mx.dev.shell.android.nycschools.flow.schools.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import mx.dev.shell.android.nycschools.core.usecase.SchoolsUseCase
import javax.inject.Inject

class SchoolListViewModelFactory @Inject constructor(
    private val useCase: SchoolsUseCase
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SchoolListViewModel(useCase) as T
    }
}