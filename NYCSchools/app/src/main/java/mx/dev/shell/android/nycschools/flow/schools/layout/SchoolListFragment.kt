package mx.dev.shell.android.nycschools.flow.schools.layout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SnapHelper
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import mx.dev.shell.android.nycschools.databinding.FragmentSchoolListBinding
import mx.dev.shell.android.nycschools.flow.schools.adapter.SchoolListAdapter
import mx.dev.shell.android.nycschools.flow.schools.vm.SchoolListViewModel
import mx.dev.shell.android.nycschools.flow.schools.vm.SchoolListViewModelFactory
import javax.inject.Inject

@AndroidEntryPoint
class SchoolListFragment : Fragment() {

    // Custom factory injection for ViewModel instance
    @Inject
    lateinit var viewModelFactory: SchoolListViewModelFactory

    // ViewBinding implementation for the list
    private lateinit var binding: FragmentSchoolListBinding

    private lateinit var vm: SchoolListViewModel

    // List Adapter with lambda onClick implementation when an item is selected
    private val schoolListAdapter = SchoolListAdapter(arrayListOf()) { itemSelected ->
        val action =
            SchoolListFragmentDirections.actionSchoolListFragmentToSchoolDetailFragment(itemSelected)
        findNavController().navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // ViewBinding instantiation
        binding = FragmentSchoolListBinding.inflate(inflater, container, false)

        setupView()
        setupViewModel()
        observeViewModel()

        return binding.root
    }

    // Initialization of the elements in the layout
    private fun setupView() {
        binding.schoolListListRec.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = schoolListAdapter
        }
    }

    // Initialization of the viewModel with the factory
    private fun setupViewModel() {
        vm = ViewModelProvider(this, viewModelFactory)
            .get(SchoolListViewModel::class.java)
    }

    // Listening of any changes provided with the mutable elements in the viewModel
    private fun observeViewModel() {
        // When the service return the shcool list, update the recycler adapter with the new one
        vm.schools.observe(this as LifecycleOwner) {
            schoolListAdapter.setNewList(it.getOrNull().orEmpty())
        }
        // When the service is called, this show the progressbar and hides it when the service return
        vm.showProgressBar.observe(this as LifecycleOwner) {
            binding.schoolListProgressbar.visibility = if (it) View.VISIBLE else View.GONE
        }
        // If there's a problem with the service, the error message will be presented in the view
        vm.errorMessage.observe(this as LifecycleOwner) {
            if (it.isNotEmpty()) {
                Snackbar.make(binding.schoolListContainer, it, Snackbar.LENGTH_SHORT).show()
            }
        }
    }
}