package mx.dev.shell.android.nycschools.core.model

import android.os.Parcel
import android.os.Parcelable

data class SchoolBo(
    val contactInfo: ContactInfoBo,
    val id: String,
    val location: LocationBo,
    val name: String,
    val schoolType: String,
    var scores: SchoolScoreBo? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(ContactInfoBo::class.java.classLoader)!!,
        parcel.readString()!!,
        parcel.readParcelable(LocationBo::class.java.classLoader)!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readParcelable(SchoolScoreBo::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(contactInfo, flags)
        parcel.writeString(id)
        parcel.writeParcelable(location, flags)
        parcel.writeString(name)
        parcel.writeString(schoolType)
        parcel.writeParcelable(scores, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolBo> {
        override fun createFromParcel(parcel: Parcel): SchoolBo {
            return SchoolBo(parcel)
        }

        override fun newArray(size: Int): Array<SchoolBo?> {
            return arrayOfNulls(size)
        }
    }
}
