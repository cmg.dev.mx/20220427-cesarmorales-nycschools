package mx.dev.shell.android.nycschools.web.service

import kotlinx.coroutines.flow.Flow
import mx.dev.shell.android.nycschools.web.model.sat.response.QuerySchoolsScoresResponseItem
import mx.dev.shell.android.nycschools.web.model.schools.response.QuerySchoolsResponseItem

interface NycSchoolsService {
    suspend fun getSchools(): Flow<Result<List<QuerySchoolsResponseItem>>>
    suspend fun getSchoolsScores(): Flow<Result<List<QuerySchoolsScoresResponseItem>>>
}
