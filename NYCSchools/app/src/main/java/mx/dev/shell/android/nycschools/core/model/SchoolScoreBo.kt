package mx.dev.shell.android.nycschools.core.model

import android.os.Parcel
import android.os.Parcelable

data class SchoolScoreBo(
    val scrRead: Int = 0,
    val scrMath: Int = 0,
    val scrWrite: Int = 0
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(scrRead)
        parcel.writeInt(scrMath)
        parcel.writeInt(scrWrite)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolScoreBo> {
        override fun createFromParcel(parcel: Parcel): SchoolScoreBo {
            return SchoolScoreBo(parcel)
        }

        override fun newArray(size: Int): Array<SchoolScoreBo?> {
            return arrayOfNulls(size)
        }
    }
}
