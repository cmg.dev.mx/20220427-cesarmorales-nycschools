package mx.dev.shell.android.nycschools.flow.schools.layout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import mx.dev.shell.android.nycschools.R
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.databinding.FragmentSchoolDetailBinding

class SchoolDetailFragment : Fragment() {

    private lateinit var binding: FragmentSchoolDetailBinding
    private val args: SchoolDetailFragmentArgs by navArgs()
    private lateinit var school: SchoolBo

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolDetailBinding.inflate(inflater, container, false)

        school = args.school

        setupView()

        return binding.root
    }

    private fun setupView() {
        binding.apply {
            schoolDetailName.text = school.name
            schoolDetailType.text = school.schoolType
            schoolDetailPhone.text = school.contactInfo.phone
            schoolDetailMail.text = school.contactInfo.email
            schoolDetailWeb.text = school.contactInfo.webSite
            schoolDetailRead.text = "${school.scores?.scrRead}"
            schoolDetailMath.text = "${school.scores?.scrMath}"
            schoolDetailWrite.text = "${school.scores?.scrWrite}"
        }
    }
}