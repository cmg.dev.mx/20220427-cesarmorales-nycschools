package mx.dev.shell.android.nycschools.core.model

import android.os.Parcel
import android.os.Parcelable

data class ContactInfoBo(
    val address: String,
    val email: String,
    val phone: String,
    val webSite: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address)
        parcel.writeString(email)
        parcel.writeString(phone)
        parcel.writeString(webSite)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContactInfoBo> {
        override fun createFromParcel(parcel: Parcel): ContactInfoBo {
            return ContactInfoBo(parcel)
        }

        override fun newArray(size: Int): Array<ContactInfoBo?> {
            return arrayOfNulls(size)
        }
    }
}
