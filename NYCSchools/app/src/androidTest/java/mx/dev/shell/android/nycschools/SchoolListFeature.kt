package mx.dev.shell.android.nycschools

import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import mx.dev.shell.android.nycschools.utils.BaseUiTest
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class SchoolListFeature: BaseUiTest() {

    @Test
    fun test001_displayScreenTitle() {
        assertDisplayed("NYC High Schools")
    }
}