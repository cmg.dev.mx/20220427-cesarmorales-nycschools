package mx.dev.shell.android.nycschools.repository

import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import mx.dev.shell.android.nycschools.core.model.ContactInfoBo
import mx.dev.shell.android.nycschools.core.model.LocationBo
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.core.model.SchoolScoreBo
import mx.dev.shell.android.nycschools.repository.mapper.SchoolMapper
import mx.dev.shell.android.nycschools.utils.BaseUnitTest
import mx.dev.shell.android.nycschools.web.model.sat.response.QuerySchoolsScoresResponseItem
import mx.dev.shell.android.nycschools.web.model.schools.response.QuerySchoolsResponseItem
import mx.dev.shell.android.nycschools.web.service.NycSchoolsService
import org.junit.Test
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
class SchoolRepositoryShould : BaseUnitTest() {

    private val service = mock(NycSchoolsService::class.java)
    private val mapper = mock(SchoolMapper::class.java)
    private val expected = listOf(
        SchoolBo(
            ContactInfoBo(
                "",
                "",
                "",
                ""
            ),
            "",
            LocationBo(0.0, 0.0),
            "",
            "",
            SchoolScoreBo(0, 0, 0)
        )
    )
    private val serviceResponse = listOf(
        QuerySchoolsResponseItem(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        )
    )
    private val serviceScoreResponse = listOf(
        QuerySchoolsScoresResponseItem(
            "",
            "",
            "",
            "",
            "",
            ""
        )
    )
    private val expectedException = RuntimeException("Something went wrong!")

    @Test
    fun querySchoolsFromService() = runTest {
        val repository = mockSuccessfulCase()
        repository.querySchools()
        verify(service, times(1)).getSchools()
    }

    @Test
    fun emitSchoolsFromService() = runTest {
        val repository = mockSuccessfulCase()
        assertEquals(expected, repository.querySchools().first().getOrNull()!!)
    }

    @Test
    fun emitFailureWhenReceiveFromService() = runTest {
        val repository = mockFailureCase()
        assertEquals(expectedException, repository.querySchools().first().exceptionOrNull()!!)
    }

    private suspend fun mockSuccessfulCase(): SchoolRepositoryImpl {
        `when`(service.getSchools()).thenReturn(
            flow {
                emit(Result.success(serviceResponse))
            }
        )

        `when`(mapper.invoke(serviceResponse)).thenReturn(expected)

        `when`(service.getSchoolsScores()).thenReturn(
            flow {
                emit(Result.success(serviceScoreResponse))
            }
        )

        return SchoolRepositoryImpl(service, mapper)
    }

    private suspend fun mockFailureCase(): SchoolRepositoryImpl {
        `when`(service.getSchools()).thenReturn(
            flow {
                emit(Result.failure(expectedException))
            }
        )
        `when`(mapper.invoke(serviceResponse)).thenReturn(expected)

        `when`(service.getSchoolsScores()).thenReturn(
            flow {
                emit(Result.success(serviceScoreResponse))
            }
        )

        return SchoolRepositoryImpl(service, mapper)
    }
}