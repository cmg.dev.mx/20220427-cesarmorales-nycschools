package mx.dev.shell.android.nycschools.web.service

import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import mx.dev.shell.android.nycschools.utils.BaseUnitTest
import mx.dev.shell.android.nycschools.web.api.NycSchoolsApi
import mx.dev.shell.android.nycschools.web.model.schools.response.QuerySchoolsResponseItem
import org.junit.Test
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
class NycSchoolsServiceShould: BaseUnitTest() {

    private val api = mock(NycSchoolsApi::class.java)
    private val expected = mock(List::class.java) as List<QuerySchoolsResponseItem>
    private val expectedException = RuntimeException("Error in the backend")

    @Test
    fun getMealsFromApi() = runTest {
        val service = mockSuccessfulCase()
        service.getSchools().first()
        verify(api, times(1)).querySchools()
    }

    @Test
    fun emitMealsFromApi() = runTest {
        val service = mockSuccessfulCase()
        TestCase.assertEquals(expected, service.getSchools().first().getOrNull())
    }

    @Test
    fun emitErrorWhenReceiveFromApi() = runTest {
        val service = mockFailureCase()
        TestCase.assertEquals("Something went wrong!", service.getSchools().first().exceptionOrNull()!!.message)
    }

    private suspend fun mockSuccessfulCase(): NycSchoolsServiceImpl {
        `when`(api.querySchools()).thenReturn(expected)
        return NycSchoolsServiceImpl(api)
    }

    private suspend fun mockFailureCase(): NycSchoolsServiceImpl {
        `when`(api.querySchools()).thenThrow(expectedException)
        return NycSchoolsServiceImpl(api)
    }
}