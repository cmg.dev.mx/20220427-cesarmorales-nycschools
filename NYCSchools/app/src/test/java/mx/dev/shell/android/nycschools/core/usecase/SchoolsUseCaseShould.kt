package mx.dev.shell.android.nycschools.core.usecase

import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.core.repository.SchoolRepository
import mx.dev.shell.android.nycschools.utils.BaseUnitTest
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

@ExperimentalCoroutinesApi
class SchoolsUseCaseShould: BaseUnitTest() {

    private val repository = mock(SchoolRepository::class.java)
    private val expectedList = mock(List::class.java) as List<SchoolBo>
    private val expectedException = RuntimeException("Something went wrong")

    @Test
    fun querySchoolsFromRepository() = runTest {
        val useCase = mockSuccessfulCase()
        useCase.querySchools()
        Mockito.verify(repository, Mockito.times(1)).querySchools()
    }

    @Test
    fun emitSchoolsFromUseCase() = runTest {
        val useCase = mockSuccessfulCase()
        TestCase.assertEquals(expectedList, useCase.querySchools().first().getOrNull())
    }

    @Test
    fun emitExceptionWhenReceiveFromRepository() = runTest {
        val useCase = mockFailureCase()
        TestCase.assertEquals(expectedException, useCase.querySchools().first().exceptionOrNull())
    }

    private suspend fun mockSuccessfulCase(): SchoolsUseCaseImpl {
        Mockito.`when`(repository.querySchools()).thenReturn(
            flow {
                emit(Result.success(expectedList))
            }
        )
        return SchoolsUseCaseImpl(repository)
    }

    private suspend fun mockFailureCase(): SchoolsUseCaseImpl {
        Mockito.`when`(repository.querySchools()).thenReturn(
            flow {
                emit(Result.failure(expectedException))
            }
        )
        return SchoolsUseCaseImpl(repository)
    }

}