package mx.dev.shell.android.nycschools.flow.schools.vm

import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import mx.dev.shell.android.nycschools.core.model.SchoolBo
import mx.dev.shell.android.nycschools.core.usecase.SchoolsUseCase
import mx.dev.shell.android.nycschools.utils.BaseUnitTest
import mx.dev.shell.android.nycschools.utils.captureValues
import mx.dev.shell.android.nycschools.utils.getValueForTest
import org.junit.Test
import org.mockito.Mockito.*
import java.lang.RuntimeException

@ExperimentalCoroutinesApi
class SchoolListViewModelShould : BaseUnitTest() {

    private val useCase = mock(SchoolsUseCase::class.java)
    private val expected = mock(List::class.java) as List<SchoolBo>
    private val exceptionExpected = RuntimeException("Something went wrong")

    @Test
    fun querySchoolsFromUseCase() = runTest {
        val viewModel = mockSuccessfulCase()
        viewModel.schools.getValueForTest()
        verify(useCase, times(1)).querySchools()
    }

    @Test
    fun emitSchoolsFromUseCase() = runTest {
        val viewModel = mockSuccessfulCase()
        assertEquals(expected, viewModel.schools.getValueForTest()?.getOrNull())
    }

    @Test
    fun emitErrorFromUseCase() = runTest {
        val viewModel = mockFailureCase()
        assertEquals(exceptionExpected.message, viewModel.schools.getValueForTest()!!.exceptionOrNull()?.message)
    }

    @Test
    fun showProgressBarWhenQuerySchools() = runTest {
        val viewModel = mockSuccessfulCase()
        viewModel.showProgressBar.captureValues {
            viewModel.schools.getValueForTest()
            assertEquals(true, values.first())
        }
    }

    @Test
    fun hideProgressBarWhenQuerySchools() = runTest {
        val viewModel = mockSuccessfulCase()
        viewModel.showProgressBar.captureValues {
            viewModel.schools.getValueForTest()
            assertEquals(false, values.last())
        }
    }

    private suspend fun mockSuccessfulCase(): SchoolListViewModel {
        `when`(useCase.querySchools()).thenReturn(
            flow {
                emit(Result.success(expected))
            }
        )
        return SchoolListViewModel(useCase)
    }

    private suspend fun mockFailureCase(): SchoolListViewModel {
        `when`(useCase.querySchools()).thenReturn(
            flow {
                emit((Result.failure(exceptionExpected)))
            }
        )
        return SchoolListViewModel(useCase)
    }
}
